((nil
  (fill-column . 70)
  (tab-width   .  8)
  (sentence-end-double-space . t))
 (scheme-mode
  (eval . (put 'stream-let 'scheme-indent-function 2))))
