;;; guile-openai --- An OpenAI API client for Guile
;;; Copyright © 2023 Andrew Whatson <whatson@tailcall.au>
;;;
;;; This file is part of guile-openai.
;;;
;;; guile-openai is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guile-openai is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with guile-openai.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (openai embedding)
  #:use-module (openai api embedding)
  #:use-module (openai client)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:export (openai-default-embedding-model

            embedding?
            embedding-vector

            openai-embedding))

(define-once openai-default-embedding-model
  (make-parameter 'text-embedding-ada-002))

(define-record-type <Embedding>
  (%make-embedding vector)
  embedding?
  (vector embedding-vector))

(define* (openai-embedding input #:key
                           (model (openai-default-embedding-model))
                           (user (openai-default-user)))
  "Send an embedding request.  Returns an embedding record.

The INPUT must be a string to be embedded.

The keyword arguments correspond to the request parameters described
in the embedding request documentation:

#:model - A symbol or string identifying the model to use.  Defaults
to `text-embedding-ada-002'.

#:user - An optional username to associate with this request."
  (let* ((model (if (symbol? model) (symbol->string model) model))
         (request (make-embedding-request model input user))
         (response (send-embedding-request request)))
    (%make-embedding
     (embedding-data-embedding
      (car (embedding-response-data response))))))
