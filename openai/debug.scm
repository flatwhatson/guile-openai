;;; guile-openai --- An OpenAI API client for Guile
;;; Copyright © 2023 Andrew Whatson <whatson@tailcall.au>
;;;
;;; This file is part of guile-openai.
;;;
;;; guile-openai is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guile-openai is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with guile-openai.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (openai debug)
  #:use-module (openai utils event-stream)
  #:use-module (openai utils json)
  #:use-module (openai utils multipart)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs bytevectors gnu)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-41)
  #:use-module (web request)
  #:use-module (web response)
  #:export (openai-last-api-call

            openai-last-request
            openai-last-request-body
            openai-last-response
            openai-last-response-body

            openai-print-last-request
            openai-print-last-response

            openai-write-last-request
            openai-write-last-response))

(define-record-type <openai-api-call>
  (%make-api-call request request-body response response-body)
  openai-api-call?
  (request       api-call-request)
  (request-body  api-call-request-body)
  (response      api-call-response)
  (response-body api-call-response-body))

(define openai-last-api-call
  (make-parameter #f (match-lambda
                       (#f
                        (%make-api-call #f #f #f #f))
                       ((request request-body response response-body)
                        (%make-api-call request request-body
                                        response response-body)))))

(define (openai-last-request)
  (api-call-request (openai-last-api-call)))

(define (openai-last-request-body)
  (api-call-request-body (openai-last-api-call)))

(define (openai-last-response)
  (api-call-response (openai-last-api-call)))

(define (openai-last-response-body)
  (api-call-response-body (openai-last-api-call)))

(define* (openai-print-last-request #:optional (port (current-output-port)))
  "Print a pretty representation of the last API request."
  (let* ((call    (openai-last-api-call))
         (request (api-call-request call))
         (body    (api-call-request-body call)))
    (cond ((not request)
           (display ";; No request\n" port))
          (else
           (write-request request port)
           (cond ((string? body)
                  (write-pretty-json body port))
                 ((pair? body)
                  (multipart-tree-visit
                   (lambda (str)
                     (put-string port str))
                   (lambda (bv)
                     ;; TODO a hexdump would be nice
                     (if (> (bytevector-length bv) 256)
                         (format port "~a\n;; ... truncated from ~a bytes\n"
                                 (bytevector-slice bv 0 256)
                                 (bytevector-length bv))
                         (format port "~a\n" bv)))
                   body)
                  (newline port)))))
    *unspecified*))

(define* (openai-print-last-response #:optional (port (current-output-port)))
  "Print a pretty representation of the last API response."
  (let* ((call     (openai-last-api-call))
         (response (api-call-response call))
         (body     (api-call-response-body call)))
    (cond ((not response)
           (display ";; No response\n" port))
          (else
           (write-response response port)
           (cond ((string? body)
                  (write-pretty-json body port))
                 ((stream? body)
                  ;; TODO truncate b64 data in image responses
                  (stream-for-each (lambda (chunk)
                                     (write-pretty-json chunk port))
                                   (line-stream->event-stream body))))))
    *unspecified*))

(define* (openai-write-last-request #:optional (port (current-output-port)))
  "Write the raw HTTP request of the last API call to PORT."
  (let* ((call    (openai-last-api-call))
         (request (api-call-request call))
         (body    (api-call-request-body call)))
    (when request
      (write-request request port)
      (when body
        (write-multipart-tree body port)))
    *unspecified*))

(define* (openai-write-last-response #:optional (port (current-output-port)))
  "Write the raw HTTP response of the last API call to PORT."
  (let* ((call     (openai-last-api-call))
         (response (api-call-response call))
         (body     (api-call-response-body call)))
    (when response
      (write-response response port)
      (cond ((string? body)
             (put-string port body))
            ((stream? body)
             (stream-for-each (lambda (chunk)
                                (put-string port chunk))
                              body))))
    *unspecified*))
