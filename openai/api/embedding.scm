;;; guile-openai --- An OpenAI API client for Guile
;;; Copyright © 2023 Andrew Whatson <whatson@tailcall.au>
;;;
;;; This file is part of guile-openai.
;;;
;;; guile-openai is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guile-openai is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with guile-openai.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (openai api embedding)
  #:use-module (openai client)
  #:use-module (json record)
  #:export (make-embedding-request
            json->embedding-request
            embedding-request->json
            embedding-request?
            embedding-request-model
            embedding-request-input
            embedding-request-user

            make-embedding-response
            json->embedding-response
            embedding-response->json
            embedding-response?
            embedding-response-object
            embedding-response-data
            embedding-response-model
            embedding-response-usage

            make-embedding-data
            json->embedding-data
            embedding-data->json
            embedding-data?
            embedding-data-object
            embedding-data-index
            embedding-data-embedding

            send-embedding-request))

;; See https://platform.openai.com/docs/api-reference/embeddings

(define-json-type <embedding-request>
  (model)
  (input)
  (user))

(define-json-type <embedding-response>
  (object)
  (data "data" #(<embedding-data>))
  (model)
  (usage "usage" <embedding-usage>))

(define-json-type <embedding-data>
  (object)
  (index)
  (embedding))

(define-json-type <embedding-usage>
  (prompt-tokens "prompt_tokens")
  (total-tokens "total_tokens"))

(define (send-embedding-request request)
  (json->embedding-response
   (openai-post-json "/v1/embeddings"
                     (embedding-request->json request))))
