;;; guile-openai --- An OpenAI API client for Guile
;;; Copyright © 2023 Andrew Whatson <whatson@tailcall.au>
;;;
;;; This file is part of guile-openai.
;;;
;;; guile-openai is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guile-openai is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with guile-openai.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (openai api edit)
  #:use-module (openai client)
  #:use-module (json record)
  #:export (make-edit-request
            json->edit-request
            edit-request->json
            edit-request?
            edit-request-model
            edit-request-input
            edit-request-instruction
            edit-request-n
            edit-request-temperature
            edit-request-top-p

            make-edit-response
            json->edit-response
            edit-response->json
            edit-response?
            edit-response-object
            edit-response-created
            edit-response-choices
            edit-response-usage

            make-edit-choice
            json->edit-choice
            edit-choice->json
            edit-choice?
            edit-choice-text
            edit-choice-index

            send-edit-request))

;; See https://platform.openai.com/docs/api-reference/edits

(define-json-type <edit-request>
  (model)
  (input)
  (instruction)
  (n)
  (temperature)
  (top-p "top_p"))

(define-json-type <edit-response>
  (object)
  (created)
  (choices "choices" #(<edit-choice>))
  (usage   "usage"   <edit-usage>))

(define-json-type <edit-choice>
  (text)
  (index))

(define-json-type <edit-usage>
  (prompt-tokens     "prompt_tokens")
  (completion-tokens "completion_tokens")
  (total-tokens      "total_tokens"))

(define (send-edit-request request)
  (openai-post-json "/v1/edits"
                    (edit-request->json request)))
