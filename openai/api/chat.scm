;;; guile-openai --- An OpenAI API client for Guile
;;; Copyright © 2023 Andrew Whatson <whatson@tailcall.au>
;;;
;;; This file is part of guile-openai.
;;;
;;; guile-openai is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guile-openai is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with guile-openai.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (openai api chat)
  #:use-module (openai client)
  #:use-module (json record)
  #:use-module (srfi srfi-41)
  #:export (make-chat-request
            json->chat-request
            chat-request->json
            chat-request?
            chat-request-model
            chat-request-messages
            chat-request-temperature
            chat-request-top-p
            chat-request-n
            chat-request-stream?
            chat-request-stop
            chat-request-max-tokens
            chat-request-presence-penalty
            chat-request-frequency-penalty
            chat-request-logit-bias
            chat-request-user

            make-chat-response
            json->chat-response
            chat-response->json
            chat-response?
            chat-response-id
            chat-response-object
            chat-response-created
            chat-response-model
            chat-response-usage
            chat-response-choices

            make-chat-usage
            json->chat-usage
            chat-usage->json
            chat-usage?
            chat-usage-prompt-tokens
            chat-usage-completion-tokens
            chat-usage-total-tokens

            make-chat-choice
            json->chat-choice
            chat-choice->json
            chat-choice?
            chat-choice-delta
            chat-choice-message
            chat-choice-finish-reason
            chat-choice-index

            make-chat-message
            json->chat-message
            chat-message->json
            chat-message?
            chat-message-role
            chat-message-content

            send-chat-request))

;; See https://platform.openai.com/docs/api-reference/chat

(define-json-type <chat-request>
  (model)
  (messages          "messages" #(<chat-message>))
  (temperature)
  (top-p             "top_p")
  (n)
  (stream?           "stream")
  (stop)
  (max-tokens        "max_tokens")
  (presence-penalty  "presence_penalty")
  (frequency-penalty "frequency_penalty")
  (logit-bias        "logit_bias")
  (user))

(define-json-type <chat-response>
  (id)
  (object)
  (created)
  (model)
  (usage   "usage"   <chat-usage>)
  (choices "choices" #(<chat-choice>)))

(define-json-type <chat-usage>
  (prompt-tokens     "prompt_tokens")
  (completion-tokens "completion_tokens")
  (total-tokens      "total_tokens"))

(define-json-type <chat-choice>
  (delta         "delta"   <chat-message>)
  (message       "message" <chat-message>)
  (finish-reason "finish_reason")
  (index))

(define-json-type <chat-message>
  (role) ;; system, user, assistant
  (content))

(define (send-chat-request request)
  (let ((result (openai-post-json "/v1/chat/completions"
                                  (chat-request->json request))))
    (if (stream? result)
        (stream-map json->chat-response result)
        (json->chat-response result))))
