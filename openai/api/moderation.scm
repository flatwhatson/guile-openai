;;; guile-openai --- An OpenAI API client for Guile
;;; Copyright © 2023 Andrew Whatson <whatson@tailcall.au>
;;;
;;; This file is part of guile-openai.
;;;
;;; guile-openai is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guile-openai is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with guile-openai.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (openai api moderation)
  #:use-module (openai client)
  #:use-module (json record)
  #:export (make-moderation-request
            json->moderation-request
            moderation-request->json
            moderation-request?
            moderation-request-input
            moderation-request-model

            make-moderation-response
            json->moderation-response
            moderation-response->json
            moderation-response?
            moderation-response-id
            moderation-response-model
            moderation-response-results

            make-moderation-result
            json->moderation-result
            moderation-result->json
            moderation-result?
            moderation-result-flagged?
            moderation-result-categories
            moderation-result-category-scores

            send-moderation-request))

;; See https://platform.openai.com/docs/api-reference/moderations

(define-json-type <moderation-request>
  (input)
  (model))

(define-json-type <moderation-response>
  (id)
  (model)
  (results "results" #(<moderation-result>)))

(define-json-type <moderation-result>
  (flagged? "flagged")
  (categories)
  (category-scores "category_scores"))

(define (send-moderation-request request)
  (json->moderation-response
   (openai-post-json "/v1/moderations"
                     (moderation-request->json request))))
