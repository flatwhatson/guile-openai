;;; guile-openai --- An OpenAI API client for Guile
;;; Copyright © 2023 Andrew Whatson <whatson@tailcall.au>
;;;
;;; This file is part of guile-openai.
;;;
;;; guile-openai is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guile-openai is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with guile-openai.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (openai)
  #:use-module (openai client)
  #:use-module (openai chat)
  #:use-module (openai completion)
  #:use-module (openai edit)
  #:use-module (openai embedding)
  #:use-module (openai image)
  #:use-module (openai moderation)
  #:re-export (openai-base-uri
               openai-api-key
               openai-organization
               openai-default-headers
               openai-default-user

               openai-default-chat-model
               openai-default-chat-temperature
               openai-default-chat-top-p
               openai-chat
               chat?
               chat-content
               chat-stream

               openai-default-image-size
               openai-default-image-format
               openai-image
               openai-image-edit
               openai-image-variation
               image?
               image-url
               image-file

               openai-default-text-edit-model
               openai-default-text-edit-temperature
               openai-default-text-edit-top-p
               openai-text-edit
               openai-default-code-edit-model
               openai-default-code-edit-temperature
               openai-default-code-edit-top-p
               openai-code-edit
               edit?
               edit-content

               openai-default-completion-model
               openai-default-completion-temperature
               openai-default-completion-top-p
               openai-completion
               completion?
               completion-content
               completion-stream

               openai-default-embedding-model
               openai-embedding
               embedding?
               embedding-vector

               openai-default-moderation-model
               openai-moderation
               moderation?
               moderation-flagged?
               moderation-categories
               moderation-scores
               moderation-score))
