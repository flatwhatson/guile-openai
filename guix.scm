;;; guile-openai --- An OpenAI API client for Guile
;;; Copyright © 2023 Andrew Whatson <whatson@tailcall.au>
;;;
;;; This file is part of guile-openai.
;;;
;;; guile-openai is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guile-openai is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with guile-openai.  If not, see
;;; <https://www.gnu.org/licenses/>.

(use-modules (guix packages)
             (guix build-system gnu)
             (guix build utils)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages imagemagick)
             (gnu packages pkg-config)
             (gnu packages tls)
             (ice-9 popen)
             (ice-9 textual-ports))

(define %source-dir
  (dirname (current-filename)))

(define %git-commit
  (with-directory-excursion %source-dir
    (get-line (open-input-pipe "git rev-parse HEAD"))))

(define guile-openai
  (package
   (name "guile-openai")
   (version (git-version "0.2" "HEAD" %git-commit))
   (source (local-file %source-dir
                       #:recursive? #t
                       #:select? (git-predicate %source-dir)))
   (build-system gnu-build-system)
   (arguments (list #:strip-binaries? #f))
   (native-inputs (list automake autoconf pkg-config))
   (inputs (list guile-3.0-latest imagemagick))
   (propagated-inputs (list guile-colorized
                            guile-gnutls
                            guile-json-4
                            guile-picture-language))
   (home-page "https://notabug.org/flatwhatson/guile-openai")
   (synopsis "Guile implementation of the OpenAI API")
   (description
    "Guile OpenAI is an implementation of the OpenAI API in Guile Scheme,
providing a convenient interface for interactive programming with their
AI models.")
   (license license:agpl3+)))

guile-openai
